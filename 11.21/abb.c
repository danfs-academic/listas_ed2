#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "abb.h"

//* MACROS
#define Greater(A, B) (A > B ? A : B)

//* STRUCTS
struct node {
    Item info;
    Bst *left, *right;
    int size;
};

//* STATIC FUNCTIONS
static Bst* create_node(Item elem) {
    Bst* node = malloc(sizeof(Bst));

    node->info  = elem;
    node->size  = 0x01;
    node->left  = NULL;
    node->right = NULL;

    return node;
}

static Bst* rotate_right(Bst* n) {
    Bst* t   = n->left;
    n->left  = t->right;
    t->right = n;

    n->size = 1 + size(n->right) + size(n->left);
    t->size = 1 + size(t->right) + size(t->left);

    return t;
}

static Bst* rotate_left(Bst* n) {
    Bst* t   = n->right;
    n->right = t->left;
    t->left  = n;

    n->size = 1 + size(n->right) + size(n->left);
    t->size = 1 + size(t->right) + size(t->left);

    return t;
}

static Bst* partition(Bst* n, int k) {
    int tl = size(n->left);
    
    if(tl > k) {
        n->left  = partition(n->left, k);
        n = rotate_right(n);
    } else if (tl < k) {
        n->right = partition(n->right, k-tl-1);
        n = rotate_left(n);
    }

    return n;
}

//* HEADER FUNCTIONS
Bst* create_bst() {
    return NULL;
}

Bst* insert_bst(Bst* bst, Item elem) {
    if(bst == NULL) return create_node(elem);
    
    //Não inserir elementos repetidos
    if(bst->info != elem) {
        if(isGreater(bst->info, elem)) {
            bst->left  = insert_bst(bst->left, elem);
            bst = rotate_right(bst);
        } else {
            bst->right = insert_bst(bst->right, elem);
            bst = rotate_left(bst);
        }
    }

    return bst;
}

Bst* insert_rand(Bst* bst, Item elem) {
    if(bst == NULL) return create_node(elem);
    
    srand(time(NULL));
    int r = rand();
    if(r < RAND_MAX/(bst->size+1)) return insert_bst(bst, elem);

    //Não inserir elementos repetidos
    if(bst->info != elem) {
        if(isGreater(bst->info, elem)) {
            bst->left  = insert_rand(bst->left, elem);
        } else {
            bst->right = insert_rand(bst->right, elem);
        }
    }

    bst->size++;

    return bst;
}

Bst* free_bst(Bst* node) {
    if(node != NULL) {
        free_bst(node->right);
        free_bst(node->left);
        free(node);
    }
    
    return NULL;
}

int height(Bst* node) {
    if(node == NULL) return -1;

    int t2 = height(node->right);
    int t1 = height(node->left);

    return Greater(t1, t2) +1;
}

int size(Bst* n) {
    if(n == NULL) return 0;

    return n->size;
}

void print_bst(Bst* node) {
    if(node == NULL) return;

    printf("(%d", node->info);
    print_bst(node->left);
    // printf(") (");
    print_bst(node->right);
    printf(") ");
}

Bst* balance(Bst* node) {
    int tam = size(node);

    if(tam < 2) return node;

    node = partition(node, tam/2);
    node->left = balance(node->left);
    node->right = balance(node->right);

    return node;
}