#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "abb.h"

int main(int argc, char const *argv[]) {
    if(argc < 1) {
        printf("Insira um tamanho");
        return 1;
    }

    int tam = atoi(argv[1]);    //Tamanho da entrada
    if(tam <= 0) {
        printf("Insira um tamanho não-nulo positivo");
        return 2;
    }

    Bst* arv = create_bst();    //Bst criada
    // int lim = tam*100;          //Limite dos valores de entrada
    // srand(time(NULL));          //Seed para a função rand

    // for(int i = 0; i < tam; i++) {
    //     arv = insert_bst(arv, rand()%lim);
    // }

    for(int i = 0; i < tam; i++) {
        arv = insert_rand(arv, i);
        if(i%100 == 0) printf("%d elementos inseridos\n", i+1);
    }
    
    printf("TAMANHO DA ÁRVORE: %d\n", size(arv));
    printf("ALTURA  DA ÁRVORE: %d\n", height(arv));
    
    arv = balance(arv);
    printf("ALTURA BALANCEADA: %d\n", height(arv));

    free_bst(arv);

    return 0;
}
