#ifndef ITEM_H_
#define ITEM_H_

//* ESPECIFICAÇÃO DO TIPO "ITEM" USADO E SUAS OPERAÇÕES BÁSICAS
typedef int Item;

#define key(A)          (A)
#define isGreater(A, B) (key(A) > key(B))
// #define exch(A, B)      { Item E; E = A; A = B; B = E; }

#endif
