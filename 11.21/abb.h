#ifndef ABB_H_
#define ABB_H_

#include "item.h"

//* Cabeçalho da Árvore Binária de Busca
typedef struct node Bst;

//* Inicializa a Binary Search Tree
Bst* create_bst();

//* Insere um Item na árvore e retorna a árvore resultante
Bst* insert_bst(Bst*, Item);

Bst* insert_rand(Bst*, Item);

//* Libera a memória da BST
Bst* free_bst(Bst*);

//* Retorna a altura da árvore
int height(Bst*);

//* Retorna o tamanho da árvore
int size(Bst*);

//* Printa a árvore
void print_bst(Bst*);

//* Printa os pesos
void print_size(Bst*);

//* Balanceia a BST
Bst* balance(Bst*);

#endif