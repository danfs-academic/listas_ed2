#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int s;
    int d;
    int t;
} Edge;

int UF_find(int* id, int a, int b) {
  return id[a] == id[b];
}

void UF_union(int* id, int a, int b, int tam) {
  int n = id[b];

  for (int i = 0; i < tam; i++) {
    if (id[i] == n) {
      id[i] = id[a];
    }
  }
}

Edge* compute_mst(Edge *a, int M, int N) {
  Edge* mst = malloc(sizeof(Edge)*N);
  int index = 0, *id = malloc(sizeof(int)*(N+2)); //Alterado o id de id[N+2] pra um malloc
  //UF_Init
  for (int j = 0; j < N+2; j++) {
    id[j] = j;
  }
  for (int i = 0; index < N; i++) {
    // printf("Tentando achar %d e %d\n", a[i].s, a[i].t);
    if (!UF_find(id, a[i].s, a[i].t)) {
      UF_union(id, a[i].s, a[i].t, N+2);
      mst[index] = a[i];
      index++;
    }
  }
  free(id);
  
  return mst;
}

int main() {
    // Le a entrada. M eh o total de arcos a serem lidos.
    int M, N;     // N eh o numero de arcos que a MST deve ter.
    scanf("%d %d\n", &M, &N);
    Edge *a = malloc(M * sizeof(Edge));
    for (int i = 0; i < M; i++) {
        scanf("%d %d %d", &(a[i].s), &(a[i].d), &(a[i].t));
    }

    // Calcula a MST. Lembre-se que o array 'a' ja esta ordenado.
    Edge *mst = compute_mst(a, M, N);

    // Exibe o resultado.
    for (int i = 0; i < N; i++) {
        printf("%d %d %d\n", mst[i].s, mst[i].d, mst[i].t);
    }

    free(a);
    free(mst);
}
