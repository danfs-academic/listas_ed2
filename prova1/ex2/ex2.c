#include <stdio.h>
#include <stdlib.h>

typedef struct {
    int s;
    int d;
    int t;
} Edge;

//Retorna positivo se 'a' > 'b', negativo se 'b' > 'a', 0 se forem iguais
int comp(Edge a, Edge b) {
  int dist = a.d - b.d;

  if (dist != 0)    return dist;
  else {
    int sdiff = a.s - b.s;

    if (sdiff != 0) return sdiff;
    else            return a.t - b.t;
  }
}

void sort(Edge *a, int N) {
  Edge temp;  //Variável temporária pra substituições

  for (int i = 0; i < N; i++) {
    for (int j = i; j > 0; j--) {
      //Se a[j-1] for maior que a[j], trocar
      if ( comp(a[j-1], a[j]) > 0 ) {
        temp = a[j];
        a[j] = a[j-1];
        a[j-1] = temp;
      } else break;
    }
  }
}

int main() {
    // Le a entrada.
    int N;
    scanf("%d\n", &N);
    Edge *a = malloc(N * sizeof(Edge));
    for (int i = 0; i < N; i++) {
        scanf("%d %d %d", &(a[i].s), &(a[i].d), &(a[i].t));
    }

    // Ordena.
    sort(a, N);

    // Exibe o resultado.
    for (int i = 0; i < N; i++) {
        printf("%d %d %d\n", a[i].s, a[i].d, a[i].t);
    }

    free(a);
}
