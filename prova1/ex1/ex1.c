#include <stdio.h>
#include <stdlib.h>

/*  Originalmente, a função f(n, k) chamava f(n-1, k),
    Agora, uso um vetor estático pra armazenar esse valor
*/
static int* vet;

int f(int n, int k) {
    if(n == 1) {
        vet[0] = 1;
    } else {
        int iter = vet[n-2];
        iter += (k-1);
        vet[n-1] = iter%n +1;
    }

    return vet[n-1];
}

int main() {
    int n, k;
    // Le a entrada.
    scanf("%d %d\n", &n, &k);
    
    vet = malloc(sizeof(int)*n);
    
    // Calcula e exibe a saida.
    for (int i = 1; i <= n; i++) {
        printf("%d %d\n", i, f(i,k));
    }

    free(vet);
}
