typedef struct {
  int src, tgt, dist;
} Edge;

#define key(A)      (A.dist)
#define less(A, B)  (key(A) < key(B))
...
