#include <stdio.h>

typedef int Item;
#define key(A)          (A)
#define less(A, B)      (key(A) < key(B))
#define exch(A, B)      { Item t = A; A = B; B = t; }
#define compexch(A, B)  if (less(B, A)) exch(A, B)

//#include "select_sort.c"
//#include "insert_sort1.c"
//#include "insert_sort2.c"
#include "shell_sort.c"

#define N 10

void print(char *msg, Item *a) {
    printf(msg);
    for (int i = 0; i < N; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

int main() {
    Item a[N] = {4, 8, 6, 3, 1, 7, 9, 2, 0, 5};
    print("ORIGINAL: ", a);
    sort(a, 0, N-1);
    print("SORTED:   ", a);
}
