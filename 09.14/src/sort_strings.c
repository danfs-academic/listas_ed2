#include <stdio.h>
#include <string.h>

typedef char* Item;
#define key(A)          (A)
#define less(A, B)      (strcmp(A,B) < 0)
#define exch(A, B)      { Item t = A; A = B; B = t; }
#define compexch(A, B)  if (less(B, A)) exch(A, B)

#include "select_sort.c"

#define N 4

void print(char *msg, Item *a) {
    printf(msg);
    for (int i = 0; i < N; i++) {
        printf("%s ", a[i]);
    }
    printf("\n");
}

int main() {
    Item a[N] = {"bb", "dddd", "ccc", "a"};
    print("ORIGINAL: ", a);
    sort(a, 0, N-1);
    print("SORTED:   ", a);
}
