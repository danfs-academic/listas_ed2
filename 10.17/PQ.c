#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "event.h"
#include "PQ.h"

#define exch(A, B)      { Event* t = A; A = B; B = t; }

// TODO: Aqui você deve implementar uma fila com prioridade mínima para
//       ordenar os eventos por tempo, isto é, o evento com o menor tempo tem
//       a maior prioridade. Veja as funções definidas em 'event.h' para
//       a manipulação das estruturas de evento. A princípio, as funções já
//       existentes em 'event.h' são suficientes para implementar a simulação,
//       isto é, você não precisa implementar mais nada lá.
//
//       Você é livre para implementar a fila com prioridade da forma que quiser
//       mas é recomendado usar um min-heap para obter um bom desempenho. As
//       simulações maiores geram uma quantidade grande de eventos: um
//       limite superior frouxo (mas seguro) para o número máximo simultâneo de
//       eventos é N^3, aonde N é o número de partículas.

struct pq {
    Event** vet;
    int tam;    //Indica a qtdd de elementos válidos E é o índice do último elemento
};

/*
 * Cria uma nova fila de prioridade mínima com o limite de elementos informado.
 */
PQ* PQ_create(int max_N) {
    PQ* heap = malloc(sizeof(PQ));

    heap->vet = malloc(sizeof(Event*)*max_N+1);
    heap->tam = 0;

    return heap;
}

/*
 * Libera a memória da fila.
 */
void PQ_destroy(PQ *pq) {
    for(int i = 1; i <= pq->tam; i++) destroy_event(pq->vet[i]);

    free(pq->vet);
    free(pq);
}

/*
 * Insere o evento na fila segundo o seu tempo.
 */
void PQ_insert(PQ *pq, Event *ev) {
    pq->tam++;
    int pos = pq->tam;

    pq->vet[pos] = ev;

    while(pos > 1) {
        Event* pai = pq->vet[pos/2];
        if(compare(ev, pai) < 0) {
            exch(ev, pai);
            pos = pos/2;
        } else break;
    }
}

/*
 * Remove e retorna o evento mais próximo.
 */
Event* PQ_delmin(PQ *pq) {
    Event* min = pq->vet[1];

    pq->vet[1] = pq->vet[pq->tam];
    int pos = 1;
    pq->tam--;
    
    Event* son;
    int pos_son;

    while(pos*2 <= pq->tam) {

        son = menor_filho(pq, pos, &pos_son);

        if(compare(pq->vet[pos], son) > 0) {
            exch(son, pq->vet[pos]);
            pos = pos_son;
        }
    }

    return min;
}

Event* menor_filho(PQ* pq, int pos, int* pos_filho) {
    if(pos*2 == pq->tam) return pq->vet[pos*2];

    Event* son1 = pq->vet[pos*2];
    Event* son2 = pq->vet[pos*2 +1];

    if(compare(son1, son2) > 0) {
        *pos_filho = pos*2 +1;
        return son2;
    } else {
        *pos_filho = pos*2;
        return son1;
    }
}

/*
 * Testa se a fila está vazia.
 */
bool PQ_is_empty(PQ *pq) {
    return pq->tam == 0;
}

/*
 * Retorna o tamanho da fila.
 */
int PQ_size(PQ *pq) {
    return pq->tam;
}
