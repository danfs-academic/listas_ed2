#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "josefo.h"

struct joseph {
  int n;
  Jose* next;
};

Jose* inicializaJose (int tam) {
  Jose* p = malloc(sizeof(*p));
  p->n = 1;

  Jose* retorno = p;
  for (int i = 1; i < tam; i++) {
    Jose* next = malloc(sizeof(*next));
    next->n = i+1;
    p = p->next = next;
  }

  p->next = retorno;

  return retorno;
}

void imprimeJose(Jose* josefo) {
  Jose* init = josefo;

  //imprime ponteiros, só pra conferir o funcionamento
  do {
    printf("%p\n", josefo);
    josefo = josefo->next;
  } while(josefo != init);
  //fim do imprime ponteiros */
}

Jose* mataJose(Jose* josefo) {
  Jose* init = josefo;

  josefo = josefo->next;

  while (josefo != init) {
    printf("Dando free no ponteiro %p, indice %d\n", josefo, josefo->n);
    free(josefo);
    josefo = josefo->next;
  }

  free(josefo);

  return NULL;
}
