#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "crivo.h"

void* criaCrivo(int n) {
  char* crivo = malloc((n-2)*sizeof(*crivo));

  for (int i = 0; i < n-2; i++) {
    crivo[i] = 1;
  }

  return crivo;
}

void* eratostenes(void* crivo, int n) {
  char* crivo2 = (char*)crivo;
  for (int i = 0; i < n-2; i++) {
    if (crivo2[i] == 1) {
      for (int j = i+1; j < n-2; j++) {
        if ((j+2)%(i+2) == 0) {
          crivo2[j] = 0;
        }
      }
    }
  }

  return crivo2;
}

void imprimeCrivo(void* crivo, int n) {
  char* crivo2 = (char*)crivo;
  for (int i = 0; i < n-2; i++) {
    if (crivo2[i] == 1) {
      printf("%d ", i+2);
    }
  }
  printf("\n");
}
