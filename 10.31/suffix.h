#if !defined(SUFFIX_H_)
#define SUFFIX_H_

#include "str.h"

typedef struct suffix Suffix;

Suffix* create_suffix(String* str, int pos);

Suffix** create_suf_array(String* str, int tam);

void sort_suf_array(Suffix** array);

void print_suffix(Suffix* suf);

Suffix** destroy_suf_array(Suffix** array);

#endif // SUFFIX_H_

