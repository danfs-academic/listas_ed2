#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include "str.h"
#include "suffix.h"

char* arq_to_string(FILE*);

int main(int argc, char const *argv[]) {
    FILE* in = fopen(argv[1], "r");

    char* aux = arq_to_string(in);
    fclose(in);
    
    String* str = create_string(aux);
    free(aux);
    // printf("A STRING É: ");
    // print_string(str);
    // printf("\n\n");

    Suffix** array = create_suf_array(str, str->len);
    for(int i = 0; i < str->len; i++) {
        print_suffix(array[i]);
        printf("\n");
    }
    printf("\n");

    sort_suf_array(array);
    for(int i = 0; i < str->len; i++) {
        print_suffix(array[i]);
        printf("\n");
    }
    printf("\n");
    
    array = destroy_suf_array(array);

    return 0;
}

char* arq_to_string(FILE* input) {
    int qtdd;
    fscanf(input, "%d\n", &qtdd);

    char* str = malloc(sizeof(char)*(qtdd+1));
    int index = 0;
    char aux;

    for(int j = 0; j < qtdd && !feof(input); j++) {
        fscanf(input, "%c", &aux);
        
        if(aux != EOF) {
            if(aux == '\n') aux = ' ';

            if( (aux != ' ' || str[index-1] != ' ') && (!isspace(aux) || aux == ' ')) {
                str[index] = aux;
                index++;
            }
        }
    }
    str[index] = '\0';

    return str;
}