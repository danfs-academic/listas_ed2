#include <stdio.h>
#include <stdlib.h>
#include "str.h"
#include "suffix.h"

struct suffix {
    String* s;
    int index;
};

Suffix* create_suffix(String* str, int pos) {
    Suffix* suf = malloc(sizeof(Suffix));

    suf->s = str;
    suf->index = pos;

    return suf;
}

Suffix** create_suf_array(String* str, int tam) {
    Suffix** array = malloc(sizeof(Suffix*)*tam);
    
    for(int i = 0; i < tam; i++) {
        array[i] = create_suffix(str, i);
    }

    return array;
}

void sort_suf_array(Suffix** array) {

}

void print_suffix(Suffix* suf) {
    for(int i = suf->index; i < suf->s->len; i++) {
        printf("%c", suf->s->c[i]);
    }
}

Suffix** destroy_suf_array(Suffix** array) {
    int tam = array[0]->s->len;

    destroy_string(array[0]->s);

    for(int i = 0; i < tam; i++) {
        free(array[i]);
    }

    free(array);

    return NULL;
}