#ifndef ABB_H_
#define ABB_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct abb Tree;

Tree* criaAbb();
Tree* insereAbb(Tree* raiz, int n);
Tree* purgeAbb(Tree* raiz);
void recPercorrePre(Tree* raiz, void(*cb)(Tree*));
void recPercorreIn(Tree* raiz, void(*cb)(Tree*));
void recPercorrePost(Tree* raiz, void(*cb)(Tree*));
int height(Tree* raiz);
int key(Tree* no);
Tree* direita(Tree*);
Tree* esquerda(Tree*);

//TESTE
void printPre(Tree*, void(*cb)(Tree*));

#endif
