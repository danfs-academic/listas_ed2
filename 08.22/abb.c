#include "abb.h"

//ESTRUTURAS
struct abb {
  int key;
  Tree *dir, *esq;
};

//FUNÇÕES ESTÁTICAS
static int maximo(int a, int b) {
  return a>b ? a : b;
}

//FUNÇÕES DO .H
Tree* criaAbb() {
  return NULL;
}

Tree* insereAbb(Tree* raiz, int n) {
  if (raiz == NULL) {
    raiz = malloc(sizeof(*raiz));
    raiz->key = n;
    raiz->dir = raiz->esq = NULL;
  } else if (raiz->key > n) {
    raiz->esq = insereAbb(raiz->esq, n);
  } else {
    raiz->dir = insereAbb(raiz->dir, n);
  }
  return raiz;
}

Tree* purgeAbb(Tree* raiz) {
  if (raiz != NULL) {
    purgeAbb(raiz->dir);
    purgeAbb(raiz->esq);
    free(raiz);
  }
  return NULL;
}

void recPercorrePre(Tree* raiz, void(*cb)(Tree*)) {
  if (raiz != NULL) {
    cb(raiz);
    recPercorrePre(raiz->esq, cb);
    recPercorrePre(raiz->dir, cb);
  }
}

void recPercorreIn(Tree* raiz, void(*cb)(Tree*)) {
  if (raiz != NULL) {
    recPercorreIn(raiz->esq, cb);
    cb(raiz);
    recPercorreIn(raiz->dir, cb);
  }
}

void recPercorrePost(Tree* raiz, void(*cb)(Tree*)) {
  if (raiz != NULL) {
    recPercorrePost(raiz->esq, cb);
    recPercorrePost(raiz->dir, cb);
    cb(raiz);
  }
}

int height(Tree* raiz) {
  if (raiz == NULL) {
    return -1;
  } else {
    return maximo(height(raiz->dir), height(raiz->esq))+1;
  }
}

int key(Tree* no) {
  return no->key;
}

Tree* direita(Tree* no) {
  return no->dir;
}

Tree* esquerda(Tree* no) {
  return no->esq;
}

//TESTE
void printPre(Tree* raiz, void(*cb)(Tree*)) {
  if (raiz != NULL) {
    printf("(");
    cb(raiz);
    printPre(raiz->esq, cb);
    printPre(raiz->dir, cb);
    printf(")");
  }
}
