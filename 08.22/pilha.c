#include "pilha.h"

struct pilha {
  Tree* pilha[100];
  int fim;
};

static int vaziaPilha(Pilha* p) {
  return (p->fim == 0) ? 1 : 0;
}

Pilha* inicializaPilha() {
  Pilha* p = malloc(sizeof(*p));
  p->fim = 0;

  return p;
}

Pilha* push(Pilha* p, Tree* no) {
  p->pilha[(p->fim)] = no;
  p->fim++;

  return p;
}

Tree* pop(Pilha* p) {
  p->fim--;

  return p->pilha[p->fim];
}

Pilha* purgePilha(Pilha* p) {
  while (p->fim != 0) {
    p->fim--;
    purgeAbb(p->pilha[p->fim]);
  }
  free(p);

  return NULL;
}

Pilha* cleanPilha(Pilha* p) {
  p->fim = 0;

  return p;
}

//IMPRIMES NÃO RECURSIVOS
void pilPercorrePre(Tree* raiz, void(*cb)(Tree*)) {
  Tree* aux = raiz;
  Pilha* pilha = inicializaPilha();

  if (aux == NULL) {
    return;
  }

  pilha = push(pilha, aux);

  while (!vaziaPilha(pilha)) {
    aux = pop(pilha);
    cb(aux);
    if (direita(aux) != NULL) {pilha = push(pilha, direita(aux));}
    if (esquerda(aux) != NULL) {pilha = push(pilha, esquerda(aux));}
  }
}

void pilPercorreIn(Tree* raiz, void(*cb)(Tree*)) {
  Tree* aux = raiz;
  Pilha* pilha = inicializaPilha();
}

void pilPercorrePost(Tree* raiz, void(*cb)(Tree*)) {

}
