#include "pilha.h"
#include <time.h>

void printContent(Tree* no);

int main(int argc, char const *argv[]) {
  int n = atoi(argv[1]); //%100 pra melhorar visualização
  srand(time(NULL));
  Tree* abb = criaAbb();

  for (int i = 0; i < n; i++) {
    abb = insereAbb(abb, rand()%100);
  }

  /*TESTE
  printf("\nARVORE MONTADA\n");
  printPre(abb, printContent);*/

  printf("\nIn Percorre Recursivo\n");
  recPercorreIn(abb, printContent);
  printf("\nIn Percorre Iterativo\n");
  pilPercorreIn(abb, printContent);
  printf("\nPré Percorre Recursivo:\n");
  recPercorrePre(abb, printContent);
  printf("\nPré Percorre Iterativo:\n");
  pilPercorrePre(abb, printContent);

  printf("\naltura = %d\n", height(abb));

  abb = purgeAbb(abb);

  return 0;
}

void printContent(Tree* no) {
  //printf("Árvore key: %d\n", key(no));
  printf("%d ", key(no));
}
