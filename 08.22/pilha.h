#ifndef PILHA_H_
#define PILHA_H_

#include "abb.h"

typedef struct pilha Pilha;

Pilha* inicializaPilha();
Pilha* push(Pilha*, Tree*);
Tree* pop(Pilha*);
Pilha* purgePilha(Pilha*);
Pilha* cleanPilha(Pilha*);

//IMPRIMES NÃO RECURSIVOS
void pilPercorrePre(Tree* raiz, void(*cb)(Tree*));
void pilPercorreIn(Tree* raiz, void(*cb)(Tree*));
void pilPercorrePost(Tree* raiz, void(*cb)(Tree*));

#endif
