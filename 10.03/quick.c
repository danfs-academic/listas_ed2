#include <stdio.h>
#include <stdlib.h>
#include "item.h"

#define CUTOFF 10

//*Partição da versão padrão do quick sort
int partition(Item* a, int ini, int fim) {
    int i = ini, j = fim+1; //posições iniciais da comparação
    Item pivo = a[ini];     //pivo da troca

    while(1) {
        while(less(a[++i], pivo)) //varre até achar um elemento maior q o pivo
            if(i == fim) break;
        while(less(pivo, a[--j])) //varre até achar um elemento menor q o pivo
            if(j == ini) break;
        if(i >= j) break;         //se houve interpolação, para as trocas

        exch(a[i], a[j]);         //troca as posições
    }
    
    exch(a[j], a[ini]);           //posiciona o pivo no local ordenado dele
    return j;                     //retorna o índice do pivo
}

//*Versão 1: quick sort clássico top-down recursivo sem nenhuma otimização. (Veja os slides 11 e 12 da Aula 06.)
void sort(Item* a, int ini, int fim) {
    if(fim <= ini) return;
    
    int pivo = partition(a, ini, fim);

    sort(a, ini, pivo-1);
    sort(a, pivo+1, fim);
}

//*Versão 2: quick sort top-down recursivo com cut-off para insertion sort. Implemente o seu código de forma que seja fácil modificar o valor de cut-off.
// Varie o valor de cut-off a partir de 1 e determine o valor ideal para a sua implementação.
void sort(Item* a, int ini, int fim) {
    if(fim <= ini-1+CUTOFF) {
        insertion_sort(a, ini, fim);
        return;
    }
    int pivo = partition(a, ini, fim);

    sort(a, ini, pivo-1);
    sort(a, pivo+1, fim);
}
