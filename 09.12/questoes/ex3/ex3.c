#include <stdio.h>
#include <stdlib.h>

#define MAX 100

float calc(int N, float* vet) {
  float sum = 0;

  for (int k = 1; k <= N; k++) {
    sum = sum + vet[k-1] + vet[N-k];
  }

  return N + (sum/N);
}

float f_C(int N) {
  float* vet = malloc(sizeof(int)*N+1);
  vet[0] = 1.000000;

  for (int i = 1; i < N+1; i++) {
    vet[i] = calc(i, vet);
  }

  return vet[N];
}

int main() {
    int N;
    // Le a entrada.
    scanf("%d\n", &N);
    // Calcula e exibe a saida.
    float res = f_C(N);
    printf("%f\n", res);
}
