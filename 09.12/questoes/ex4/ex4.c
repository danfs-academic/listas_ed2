#include <stdio.h>
#include <stdlib.h>

#define M 100
#define N 18

//Compara duas strings
int comp(char a[N], char b[N]) {
  int i = 0;            //Variável índice
  int dif = b[i]-a[i];  //Variável diferença entre strings
  //Enquanto não houver diferença
  while (dif == 0 && i <= N) {
    //Incrementa o indice de comparação e recalcula a diferença
    i++;
    dif = b[i]-a[i];
  }
  //Retorna a diferença ( >0 = b maior; =0 = iguais; <0 = a maior)
  return dif;
}

//Troca os chars de duas strings
void exchange(char a[N], char b[N]) {
  char aux; //Variável auxiliar
  //Loop de troca para todos os 18 elementos
  for (int i = 0; i < N; i++) {
    aux = a[i];
    a[i] = b[i];
    b[i] = aux;
  }
}

void sort(char a[M][N]) {
  //Pra cada elemento do vetor
  for (int i = 1; i < M; i++) {
    //printf("LOOP Nº %d\n", i); //TESTE
    //Ir lendo para trás no vetor, até o elemento originalmente 'i' fique ordenado
    int j = i;
    while((comp(a[j], a[j-1]) > 0) && j>0) {
      exchange(a[j], a[j-1]);
      j--;
    }
  }
}

int main() {
    char a[M][N];
    // Le a entrada.
    for (int i = 0; i < M; i++) {
        fscanf(stdin,"%s", a[i]);
    }
    // Ordena.
    sort(a);
    // Exibe o resultado.
    for (int i = 0; i < M; i++) {
        printf("%s\n", a[i]);
    }
}
